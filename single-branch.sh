#!/bin/bash
################################################################################
# Esse script foi criado para auxiliar na criação das
# branchs dos diferentes ambientes do singlebet e facilitar abertura de MRs.
#
# Para executar o script, basta rodar o comando abaixo:
# git pull && sudo chmod +x ./single-branch.ch && ./single-branch.sh
#
# Caso esteja usando mac ou linux, você pode criar um alias para o script.
# Para isso, basta adicionar a linha abaixo no seu arquivo .bashrc ou .zshrc:
# alias single-branch="sudo chmod +x ./local/do/script/single-branch.sh && ./local/do/script/single-branch.sh"
#
# Não se esqueça sempre de verificar se o script está atualizado antes de executá-lo.
# Para isso, basta rodar o comando abaixo:
# git pull
#
# Descrição de funcionalidades:
# O script recebe o nome da branch base (criada a partir de prod)
# e, em seguida, irá criar as branchs de dev e stg com o mesmo padrão de nome.
#
# Após a criação das branchs, o script irá perguntar se você deseja abrir um MR
# para a branch de dev. (futuramente)
################################################################################

######################################
# DIRETÓRIO DO PROJETO
######################################

DIR="$HOME/PixBet/singlebet" # <-- altere para diretório do singlebet no seu sistema

######################################
# WELCOME
######################################

echo ' __          __        ___  __   __             __       '
echo '/__` | |\ | / _` |    |__  |__) |__)  /\  |\ | /  ` |__| '
echo '.__/ | | \| \__> |___ |___ |__) |  \ /~~\ | \| \__, |  | '

echo ""

echo "Bem-vindo ao single-branch.sh!"
echo ""
echo "Este é um script para auxiliar na criação das branchs dos diferentes ambientes do singlebet e facilitar abertura de MRs."
echo ""

echo "------------------------------------------------------"

######################################
# DEFINIÇÕES INICIAIS
######################################

# Sai em caso de erro
set -e

######################################
# FUNÇÕES AUXILIARES
######################################

# Função para exibir o cabeçalho
function print_header() {
  printf "\n%s\n" "> $1"
  printf "%s\n" "  -------------"
}

# Função para exibir erro e dica e depois sair
function print_error_tip_and_exit() {
  echo "  Erro: $1"
  echo "  $2"
  echo "  Saindo..."
  exit 1
}

# Função para exibir sucesso e continuar
function print_success_and_continue() {
  echo "  Sucesso! $1"
  echo "  Continuando..."
}

# Função para perguntar ao usuário se ele concorda com uma ação
function ask_yes_no() {
  local message="$1"
  local response
  # Loop enquanto o usuário não digitar uma resposta válida
  while true; do
    read -p "$message (s/n): " response
    case "$response" in
    [Ss]*) return 0 ;; # Retorna 0 se o usuário digitar "s"
    [Nn]*) return 1 ;; # Retorna 1 se o usuário digitar "n"
    *) echo "  Resposta inválida. Digite s ou n." ;;
    esac
  done
}

# Função para forçar a atualização de uma branch
force_pull() {
  local branch=$1

  # Fazer checkout da branch
  git checkout $branch

  echo "  Resetando a branch $branch para a versão remota..."
  git reset --hard origin/$branch

  git pull --rebase
}

######################################
# VALIDAÇÃO DO DIRETÓRIO DO PROJETO
######################################

print_header "Verificando o diretório do projeto"

echo "  Diretório do projeto no script: $DIR"

# Pergunta ao usuário se o diretório está correto
if ! ask_yes_no "  Esse diretório está correto?"; then
  printf "%s\n" "  -------------"
  read -p "  Digite o caminho do diretório (ou altere diretamente no script): " DIR
  echo "  Diretório informado: $DIR"
fi

######################################
# VALIDAÇÃO DE DIRETÓRIO
######################################

print_header "Verificando se o diretório existe"

if [[ -d "$DIR" ]]; then
  print_success_and_continue "O diretório existe."
else
  print_error_tip_and_exit \
    "Esse diretório não existe!" \
    "Por favor, informe um diretório válido. (Lembre-se que letras maiúsculas e minúsculas são importantes!)"
fi

# Muda para o diretório do projeto
cd $DIR

######################################
# VERIFICAÇÃO DE PARÂMETROS
######################################

print_header "Verificando se foi informado o nome da branch"

if [ ! -z "$1" ]; then
  print_success_and_continue "O nome da branch foi informado. Branch: \"$1\""
else
  print_error_tip_and_exit \
    "Você precisa informar o nome da branch!" \
    "Exemplo: ./pix-mr.sh feature/pixf-123"
fi

BRANCH_NAME="$1"

NEW_BRANCH_NAMES="(À seguir, as branches criadas terão o nome \"$BRANCH_NAME-dev\" e \"$BRANCH_NAME-stg\")"

print_header "Verificando se o nome da branch segue o padrão correto"

# Verifica se o nome da branch segue o padrão: feature/bugfix/refactor / seguido de pixf- e uma numeração
echo "  Exemplos: feature/pixf-123 ou bugfix/pixf-123 ou refactor/pixf-123"
echo "  ---"
if [[ $BRANCH_NAME =~ ^(feature|bugfix|refactor)/pixf-[0-9]+$ ]]; then
  print_success_and_continue "A branch \"$BRANCH_NAME\" segue o padrão de nome correto."
  echo "  $NEW_BRANCH_NAMES"
else
  echo "  A branch \"$BRANCH_NAME\" não segue o padrão de nome correto."
  echo "  $NEW_BRANCH_NAMES"
  echo ""

  # Pergunta ao usuário se ele quer continuar mesmo assim
  if ask_yes_no "  Deseja continuar mesmo assim?"; then
    echo "  Continuando com a branch \"$BRANCH_NAME\"..."
  else
    echo "  Saindo..."
    exit 1
  fi
fi

######################################
# VERIFICAÇÕES DO REPOSITÓRIO LOCAL
######################################

print_header "Verificando se há arquivos em stage"

if git diff --cached --exit-code >/dev/null; then
  print_success_and_continue "O repositório local não contém alterações."
else
  ERROR_AND_TIP=("O repositório local contém alterações!" "Por favor, faça um commit ou um stash das alterações antes de executar o script.")
  print_error_tip_and_exit "${ERROR_AND_TIP[@]}"
fi

######################################
# VERIFICAÇÕES DA BRANCH
######################################

print_header "Verificando se a branch \"$BRANCH_NAME\" existe"

if git rev-parse --verify $BRANCH_NAME >/dev/null 2>&1; then
  print_success_and_continue "A branch \"$BRANCH_NAME\" existe."
else
  if ask_yes_no "A branch \"$BRANCH_NAME\" não existe. Deseja criá-la à partir de prod?"; then
    echo "  Criando a branch \"$BRANCH_NAME\"..."
    echo "-----"
    git checkout prod && git pull && git checkout -b ${BRANCH_NAME} && git checkout -
    echo "-----"
    print_success_and_continue "A branch \"$BRANCH_NAME\" foi criada."

    if ask_yes_no "  Deseja fazer o push da branch \"$BRANCH_NAME\"?"; then
      echo "  Fazendo o push da branch \"$BRANCH_NAME\"..."
      echo "-----"
      git push -u origin $BRANCH_NAME
      echo "-----"
      print_success_and_continue "O push da branch \"$BRANCH_NAME\" foi feito."
    else
      echo "  Não foi feito o push da branch \"$BRANCH_NAME\"."
    fi
  else
    echo "  Saindo..."
    exit 1
  fi
fi

# ------------------------------------

print_header "Verificando se a branch \"$BRANCH_NAME\" foi criada à partir de \"prod\""

if [ "$(
  git merge-base --is-ancestor prod $BRANCH_NAME
  echo $?
)" -eq 0 ]; then
  print_success_and_continue "A branch $BRANCH_NAME foi criada à partir de prod."
else
  ERROR_AND_TIP=("A branch \"$BRANCH_NAME\" não foi criada à partir de \"prod\"!" "Por favor, para usar o script corretamente, informe uma branch que foi criada à partir da branch \"prod\".")
  print_error_tip_and_exit "${ERROR_AND_TIP[@]}"
fi

# ------------------------------------

print_header "Verificando se a branch \"$BRANCH_NAME\" está atualizada em relação ao repositório remoto"

if git diff --quiet origin/$BRANCH_NAME $BRANCH_NAME; then
  print_success_and_continue "A branch $BRANCH_NAME está atualizada."
else
  ERROR_AND_TIP=("A branch \"$BRANCH_NAME\" não está atualizada no repositório remoto!" "Por favor, faça um pull, se necessário, resolva os conflitos, e faça um push da branch para o repositório remoto.")
  print_error_tip_and_exit "${ERROR_AND_TIP[@]}"
fi

# ------------------------------------

print_header "Verificando se a branch \"$BRANCH_NAME\" contém alterações"

if ! git diff --quiet $BRANCH_NAME origin/prod; then
  print_success_and_continue "A branch \"$BRANCH_NAME\" está à frente da branch prod."
else
  ERROR_AND_TIP=("A branch \"$BRANCH_NAME\" não contém alterações!" "Por favor, faça um commit antes de executar o script.")
  print_error_tip_and_exit "${ERROR_AND_TIP[@]}"
fi

# ------------------------------------

print_header "Verificando se a branch \"$BRANCH_NAME\" contém conflitos em relação à branch prod"

if ! git merge-base --all prod $BRANCH_NAME | grep -q $BRANCH_NAME; then
  print_success_and_continue "A branch \"$BRANCH_NAME\" não contém conflitos com prod."
else
  ERROR_AND_TIP=("A branch \"$BRANCH_NAME\" contém conflitos em relação à branch prod!" "Por favor, faça um merge com prod e resolva os conflitos e depois execute o script novamente.")
  print_error_tip_and_exit "${ERROR_AND_TIP[@]}"
fi

######################################
# VERIFICAÇÕES DE BRANCHES ADICIONAIS
######################################

print_header "Verificando se as branches \"$BRANCH_NAME-dev\", e \"$BRANCH_NAME-stg\" existem"

# Verifica se a branch "$BRANCH_NAME-dev" existe
if ! git branch | grep -q "$BRANCH_NAME-dev"; then
  print_success_and_continue "A branch \"$BRANCH_NAME-dev\" não existe."
else
  ERROR_AND_TIP=("A branch \"$BRANCH_NAME-dev\" já existe!" "Para o script funcionar corretamente, as branches \"$BRANCH_NAME-dev\" e \"$BRANCH_NAME-stg\" não podem existir.")
  print_error_tip_and_exit "${ERROR_AND_TIP[@]}"
fi

# Verifica se a branch "$BRANCH_NAME-stg" existe
if ! git branch | grep -q "$BRANCH_NAME-stg"; then
  print_success_and_continue "A branch \"$BRANCH_NAME-stg\" não existe."
else
  ERROR_AND_TIP=("A branch \"$BRANCH_NAME-stg\" já existe!" "Para o script funcionar corretamente, as branches \"$BRANCH_NAME-dev\" e \"$BRANCH_NAME-stg\" não podem existir.")
  print_error_tip_and_exit "${ERROR_AND_TIP[@]}"
fi

######################################
# CRIAÇÃO DE BRANCHES ADICIONAIS
######################################

print_header "Criando as branches \"$BRANCH_NAME-dev\" e \"$BRANCH_NAME-stg\""

# Força a atualização da branch "develop" e cria a branch "${BRANCH_NAME}-dev"
force_pull develop
git branch "${BRANCH_NAME}-dev"
print_success_and_continue "A branch \"${BRANCH_NAME}-dev\" foi criada."

# Força a atualização da branch "staging" e cria a branch "${BRANCH_NAME}-stg"
force_pull staging
git branch "${BRANCH_NAME}-stg"
print_success_and_continue "A branch \"${BRANCH_NAME}-stg\" foi criada."

######################################
# ATUALIZAÇÃO DE BRANCHES ADICIONAIS
######################################

print_header "Atualizando as branches \"$BRANCH_NAME-dev\" e \"$BRANCH_NAME-stg\""

# Muda para a branch de origem e pega todos os novos commits
git checkout "$BRANCH_NAME"
new_commits=$(git log --reverse --pretty=format:"%H" prod..HEAD)

# Muda para a branch de dev
git checkout $BRANCH_NAME-dev

# Refaz todos os commits
for commit in $new_commits; do
  git cherry-pick $commit
done

# Muda para a branch de stg
git checkout $BRANCH_NAME-stg

# Refaz todos os commits
for commit in $new_commits; do
  git cherry-pick $commit
done

# Volta para a branch original
git checkout "${BRANCH_NAME}"

echo -e "\033[32mO script foi executado com sucesso. As branches ${BRANCH_NAME}-dev e ${BRANCH_NAME}-stg foram criadas e todos os commits da branch ${BRANCH_NAME} foram refeitos nelas.\033[0m"

if ask_yes_no "Deseja fazer push das branches?"; then
  git push origin ${BRANCH_NAME}-dev
  git push origin ${BRANCH_NAME}-stg
  git push origin ${BRANCH_NAME}
  echo "Push realizado com sucesso!"
else
  echo "Ok! Script finalizado."
fi

exit 0
